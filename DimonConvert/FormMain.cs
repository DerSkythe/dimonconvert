﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using DimonConvert.Properties;

namespace DimonConvert
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Settings.Default.LastFolder = Path.GetDirectoryName(openFileDialog.FileName);
                    Settings.Default.Save();
                    var convertedFile = new StringBuilder();
                    var reg = new Regex(@"^(.*?\|)(.*?)\|(.*)\|");
                    using (var streamReader = new StreamReader(openFileDialog.FileName))
                    {
                        string line;
                        int i = 0;
                        while ((line = streamReader.ReadLine()) != null)
                        {
                            if (i > 0)
                            {
                                var m = reg.Match(line);
                                convertedFile.Append(m.Groups[1]).Append(i).Append("|").Append(m.Groups[3])
                                        .Append("|102|").Append(m.Groups[2]).Append("|").Append("\r\n");
                            }
                            else
                            {
                                convertedFile.Append(line).Append("\r\n");
                            }

                            i++;
                        }

                        streamReader.Close();
                    }

                    var ext = Path.GetExtension(openFileDialog.FileName);
                    var file = Path.GetFileNameWithoutExtension(openFileDialog.FileName);

                    File.WriteAllText(
                                      Settings.Default.LastFolder +
                                      Path.DirectorySeparatorChar + file + "_ready" + ext, convertedFile.ToString());
                }
                catch (Exception exp)
                {
                    ShowError(exp.Message);
                }
            }
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            if (Settings.Default.LastFolder == String.Empty || !Directory.Exists(Settings.Default.LastFolder))
            {
                Settings.Default.LastFolder = Directory.GetCurrentDirectory();
            }
            openFileDialog.Multiselect = false;
            openFileDialog.InitialDirectory = Settings.Default.LastFolder;
            Settings.Default.Save();
        }

        private void ShowError(string message)
        {
            MessageBox.Show(message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }
    }
}
